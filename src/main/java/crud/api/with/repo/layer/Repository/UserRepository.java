package crud.api.with.repo.layer.Repository;

import crud.api.with.repo.layer.Domain.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
