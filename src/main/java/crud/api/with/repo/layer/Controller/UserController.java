package crud.api.with.repo.layer.Controller;

import crud.api.with.repo.layer.Domain.AuthenicationResponse;
import crud.api.with.repo.layer.Domain.User;
import crud.api.with.repo.layer.Repository.UserRepository;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserRepository repository;

    public UserController(UserRepository repository) {
        this.repository = repository;
    }

    @GetMapping("")
    public Iterable<User> all() {
        return this.repository.findAll();
    }

    @PostMapping("")
    public User create(@RequestBody User user) {
        return this.repository.save(user);
    }

    @GetMapping("/{id}")
    public User getLessonById(@PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        User user = repository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this id :: " + userId));
        return user;
    }

    @PatchMapping("{id}")
    public User patch(@PathVariable(value = "id") Long userId, @RequestBody Map requestParams) {
        return repository.findById(userId).map(user -> {
            if (requestParams.get("email") != null) {
                user.setEmail((String) requestParams.get("email"));
            }
            if (requestParams.get("password") != null) {
                user.setPassword((String) requestParams.get("password"));
            }
            return repository.save(user);
        }).orElseGet(() -> {
            return null;
        });
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        User user = repository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("Lesson not found for this id :: " + userId));

        repository.delete(user);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @PostMapping("/authenticate")
    public AuthenicationResponse authenticateUser(@RequestBody Map requestBody)
            throws ResourceNotFoundException {

        Iterator<User> userIterator = repository.findAll().iterator();
        while(userIterator.hasNext()) {
            User localUser = userIterator.next();
            if ((requestBody.get("email") != null && localUser.getEmail().equals(requestBody.get("email"))) &&
                    (requestBody.get("email") != null && localUser.getPassword().equals(requestBody.get("password")))){
                return new AuthenicationResponse(true, localUser);
            }
        }
        return new AuthenicationResponse(false, null);
    }
}
