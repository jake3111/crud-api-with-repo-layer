package crud.api.with.repo.layer.Domain;

public class AuthenicationResponse {
    private boolean authenticated;
    private User user;

    public AuthenicationResponse(boolean authenticated ,User user){
        this.authenticated = authenticated;
        this.user = user;
    }


    public boolean isAuthenticated() {
        return authenticated;
    }

    public User getUser() {
        return user;
    }
}
